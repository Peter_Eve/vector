package vector

import (
	"math"
	//"fmt"
)

type Vector struct {
	X float64
	Y float64
}

type Line struct {
	O Vector
	E Vector
}

func IntVector(x int, y int) Vector { //Creates a vector from integers
	return Vector{float64(x), float64(y)}
}

func Add (v1 Vector, v2 Vector) Vector { //Adds v1 to v2
	return Vector{v1.X + v2.X, v1.Y + v2.Y}
}

func Sub (v1 Vector, v2 Vector) Vector { //Subtracts v1 from v1
	return Vector{v1.X - v2.X, v1.Y - v2.Y}
}

func Mul (v1 Vector, v2 Vector) Vector { //Multiplies v1 by v2
	return Vector{v1.X * v2.X, v1.Y * v2.Y}
}

func Div (v1 Vector, v2 Vector) Vector { //Divides v1 by v2
	return Vector{v1.X / v2.X, v1.Y / v2.Y}
}

func Abs (v Vector) Vector { //Absolute of vector v
	X := math.Abs(v.X)
	Y := math.Abs(v.Y)
	return Vector{X, Y}
}

func Mag (v Vector) float64 { //Magnitude of vector v
	return math.Sqrt((v.X*v.X) + (v.Y*v.Y))
}

func Nor (v Vector) Vector { //Normalises vector v
	hyp := 1 / Mag(v)
	return Vector{v.X*hyp, v.Y*hyp}
}

func Rnd (v Vector) Vector { //Rounds the values of vector v
	return Vector{math.Round(v.X), math.Round(v.Y)}
}

func Rot (v1 Vector, v2 Vector) float64 { //Angle between v1 and v2
	angle := math.Atan2(v2.Y - v1.Y, v2.X - v1.X)
	angle += (math.Pi / 2)
	if angle < 0 {
		angle +=  2 * math.Pi
	}
	return angle
}

func Unt (v1 Vector, v2 Vector, d float64) Vector { //A point d units between v1 and v2
	unit := Nor(Sub(v2, v1))
	return Vector{unit.X*d, unit.Y*d}
}

func Cep (va1 *Vector, va2 *Vector, vb1 *Vector, vb2 *Vector) Vector { //returns where va1-va2 intercects vb1-vb2
	return Vector{0,0}
}

func Intercept (a Line, b Line) bool { //Returns whether a-b intercepts c-d
	x1 := a.O.X
	x2 := a.E.X
	x3 := b.O.X
	x4 := b.E.X

	y1 := a.O.Y
	y2 := a.E.Y
	y3 := b.O.Y
	y4 := b.E.Y

	ta1 := ((y3-y4)*(x1-x3)+(x4-x3)*(y1-y3))
	ta2 := ((x4-x3)*(y1-y2)-(x1-x2)*(y4-y3))
	if ta2 == 0 { //colinear or equal
		return false
	}

	tb1 := ((y1-y2)*(x1-x3)+(x2-x1)*(y1-y3))
	tb2 := ((x4-x3)*(y1-y2)-(x1-x2)*(y4-y3))
	if tb2 == 0 { //colinear or equal
		return false
	}

	ta := ta1/ta2
	tb := tb1/tb2
	if (ta >= 0 && ta <= 1 && tb >= 0 && tb <= 1) {
		return true
	} else {
		return false
	}
}

func Invert (v Vector) Vector {
	return Vector{v.X*-1,v.Y*-1}
}